#!/bin/bash

# GHC tests
CWD=$PWD
rm /tmp/hello*
cat >>/tmp/hello.hs <<EFO
main = putStrLn "Hello, World!"
EFO

printf "Compile hello.hs..."
ghc -o /tmp/hello /tmp/hello.hs >/dev/null 2>&1 || { printf "FAILED!\n"; exit 1; }
printf "...PASSED\n"
printf "Check byte-compiled hello program is executable..."
[ -x /tmp/hello ] || { printf "FAILED!\n"; exit 1; }
printf "PASSED\n"
printf "Test results of executing compiled test program..."
res=$(/tmp/hello)
[ "$res"="Hello, World!" ] || { printf "FAILED!\n"; exit 1; }
printf "PASSED\n\nALL TESTS PASSED\n"
