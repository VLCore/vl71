#!/bin/sh

# Copyright 2008, 2009  Patrick J. Volkerding, Sebeka, MN, USA
# All rights reserved.
#
# Redistribution and use of this script, with or without modification, is
# permitted provided that the following conditions are met:
#
# 1. Redistributions of this script must retain the above copyright
#    notice, this list of conditions and the following disclaimer.
#
#  THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR IMPLIED
#  WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
#  MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO
#  EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
#  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
#  PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
#  OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
#  WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
#  OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
#  ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
NAME="gftp"
VERSION=${VERSION:-"2.0.19"}
VER=$(echo $VERSION|sed 's/-/_/') #this fixes - in version
VL_PACKAGER=${VL_PACKAGER:-"M0E-lnx"}   #Enter your Name!
LINK=${LINK:-"http://www.gftp.org/$NAME-$VERSION.tar.bz2"}  #Enter URL for package here!


#SYSTEM VARIABLES
#----------------------------------------------------------------------------
BUILDNUM=${BUILDNUM:-"2"}
VL_VERSION=${VL_VERSION:-"$(ls /var/log/packages/|grep vlconfig2|cut -d "-" -f4|cut -c 2-5)"}
BUILD=${BUILD:-"$BUILDNUM""$VL_VERSION"}
ARCH=${ARCH:-"$(uname -m)"}
CONFIG_OPTIONS=${CONFIG_OPTIONS:-""}
LDFLAG_OPTIONS=${LDFLAG_OPTIONS:-""}
ADDRB=${ADDRB:-""} #Add deps that need to be added to the slack-required file here
EXRB=${EXRB:-""} #Add deps that need to be excluded from the slack-required file here
MAKEDEPENDS=${MAKEDEPENDS:-"slapt-get openssl curl gtk+2 harfbuzz"}


if [ "$ARCH" = "i?86" ]; then
  SLKCFLAGS="-O2 -march=i586 -mtune=i686"
elif [ "$ARCH" = "s390" ]; then
  SLKCFLAGS="-O2"
elif [ "$ARCH" = "x86_64" ]; then
  SLKCFLAGS="-O2 -fPIC"
fi

CWD=$(pwd)
TMP=${TMP:-/tmp}
PKG=$TMP/package-gftp
RELEASEDIR=${RELEASEDIR:-"$CWD/.."}

if [ "$NORUN" != 1 ]; then
	# Download source
	(
	cd $CWD
	for link in $(echo $LINK); do
		wget -c --no-check-certificate $link
	done
	)


rm -rf $PKG
mkdir -p $TMP $PKG

cd $TMP
rm -rf gftp-$VERSION
tar xvf $CWD/gftp-$VERSION.tar.bz2 || exit 1
cd gftp-$VERSION || exit 1
chown -R root:root .
find . \
  \( -perm 777 -o -perm 775 -o -perm 711 -o -perm 555 -o -perm 511 \) \
  -exec chmod 755 {} \; -o \
  \( -perm 666 -o -perm 664 -o -perm 600 -o -perm 444 -o -perm 440 -o -perm 400 \) \
  -exec chmod 644 {} \;

CFLAGS="$SLKCFLAGS" \
./configure \
  --prefix=/usr \
  --sysconfdir=/etc \
  --localstatedir=/var/lib \
  --mandir=/usr/man \
  --docdir=/usr/doc/gftp-$VERSION \
  --build=$ARCH-slackware-linux || exit 1

make || exit 1
make install DESTDIR=$PKG || exit 1

find $PKG | xargs file | grep -e "executable" -e "shared object" \
  | grep ELF | cut -f 1 -d : | xargs strip --strip-unneeded 2> /dev/null

# Put symlinks to the themable icons in place
mkdir -p $PKG/usr/share/icons/hicolor/{scalable,16x16,22x22,24x24,32x32,48x48}/apps
ln -s /usr/share/gftp/gftp-scalable.svg \
  $PKG/usr/share/icons/hicolor/scalable/apps/gftp.svg
for i in 16x16 22x22 24x24 32x32 48x48 ; do
  ln -s /usr/share/gftp/gftp-${i}.png \
    $PKG/usr/share/icons/hicolor/${i}/apps/gftp.png
done

mkdir -p $PKG/usr/doc/gftp-$VERSION
cp -a \
  AUTHORS COPYING NEWS README README.html THANKS TODO docs/USERS-GUIDE \
  $PKG/usr/doc/gftp-$VERSION

( cd $PKG/usr/man
  find . -type f -exec gzip -9 {} \;
  for i in $(find . -type l) ; do ln -s $(readlink $i).gz $i.gz ; rm $i ; done
)

mkdir -p $PKG/install
cat $CWD/slack-desc > $PKG/install/slack-desc
cat $CWD/slack-desc > $RELEASEDIR/slack-desc


cd $PKG
echo "Finding dependencies..."
ADD="$ADDRB" EXCLUDE="$EXRB" requiredbuilder -v -y -s $RELEASEDIR $PKG
echo "Creating package $NAME-$VERSION-$ARCH-$BUILD.txz"

/sbin/makepkg -l y -c n $RELEASEDIR/$NAME-$VERSION-$ARCH-$BUILD.txz
fi

