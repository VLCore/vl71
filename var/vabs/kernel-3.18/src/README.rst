WHEN UPDATING THIS PACKAGE
==========================

After updating this package, Other packages that that contain kernel
modules will also need to be re-built.  The list of packages includes
but is not limited to 

  * broadcom-sta
  * ndiswrapper
  * aufs3-util
  * bcm_wimax
  * amd-catalyst-driver

All of these must be built for the specific kernel version


