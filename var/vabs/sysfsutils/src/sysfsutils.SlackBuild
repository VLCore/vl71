#!/bin/sh

# Copyright 2008, 2009, 2010  Patrick J. Volkerding, Sebeka, Minnesota, USA
# All rights reserved.
#
# Redistribution and use of this script, with or without modification, is
# permitted provided that the following conditions are met:
#
# 1. Redistributions of this script must retain the above copyright
#    notice, this list of conditions and the following disclaimer.
#
#  THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR IMPLIED
#  WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
#  MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO
#  EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
#  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
#  PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
#  OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
#  WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
#  OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
#  ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

NAME=sysfsutils
VERSION=2.1.0
LINK=${LINK:-"http://slackware.osuosl.org/slackware-current/source/a/$NAME/$NAME-$VERSION.tar.bz2"}

#SYSTEM VARIABLES
#----------------------------------------------------------------------------
BUILDNUM=${BUILDNUM:-"1"}
VL_VERSION=${VL_VERSION:-"$(ls /var/log/packages/|grep vlconfig2|cut -d "-" -f4|cut -c 2-5)"}
BUILD=${BUILD:-"$BUILDNUM""$VL_VERSION"}
ARCH=${ARCH:-"$(uname -m)"}
CONFIG_OPTIONS=${CONFIG_OPTIONS:-""}
LDFLAG_OPTIONS=${LDFLAG_OPTIONS:-""}
ADDRB=${ADDRB:-""} #Add deps that need to be added to the slack-required file here
EXRB=${EXRB:-""} #Add deps that need to be excluded from the slack-required file here
MAKEDEPENDS=${MAKEDEPENDS:-""} #Add deps needed TO BUILD this package here.
#----------------------------------------------------------------------------

# DO NOT EXECUTE if NORUN is set to 1
if [ "$NORUN" != "1" ]; then

#CFLAGS SETUP
#--------------------------------------------
if [[ "$ARCH" = i?86 ]]; then
  ARCH=i586
  SLKCFLAGS="-O2 -march=i586 -mtune=i686"
  CONFIGURE_TRIPLET="i586-vector-linux"
  LIBDIRSUFFIX=""
elif [ "$ARCH" = "x86_64" ]; then
  SLKCFLAGS="-O2 -fpic"
  CONFIGURE_TRIPLET="x86_64-vlocity-linux"
  LIBDIRSUFFIX="64"
elif [ "$ARCH" = "powerpc" ]; then
  SLKCFLAGS="-O2"
  CONFIGURE_TRIPLET="powerpc-vlocity-linux"
  LIBDIRSUFFIX=""
fi

export CFLAGS="$SLKCFLAGS $CFLAG_OPTIONS"
export CXXFLAGS=$CFLAGS
export LDFLAGS="$LDFLAGS $LDFLAG_OPTIONS"
#--------------------------------------------

#get the source..........
for SRC in $(echo $LINK);do
if [ ! -f $CWD/$(basename $SRC) ]
then
	wget --no-check-certificate -c $SRC
fi
done
CWD=$(pwd)
cd ../
RELEASEDIR=$(pwd)
cd $CWD
mkdir -p $RELEASEDIR/tmp
TMP=$RELEASEDIR/tmp
PKG=$TMP/package-sysfsutils

rm -rf $PKG
mkdir -p $TMP $PKG
cd $TMP
rm -rf sysfsutils-$VERSION
tar xjvf $CWD/sysfsutils-$VERSION.tar.bz2
cd sysfsutils-$VERSION
chown -R root:root .
find . \
  \( -perm 777 -o -perm 775 -o -perm 711 -o -perm 555 -o -perm 511 \) \
  -exec chmod 755 {} \; -o \
  \( -perm 666 -o -perm 664 -o -perm 600 -o -perm 444 -o -perm 440 -o -perm 400 \) \
  -exec chmod 644 {} \;

./configure \
  --prefix=/usr \
  --libdir=/lib${LIBDIRSUFFIX} \
  --enable-static=no \
  --build=$CONFIGURE_TRIPLET \
  $CONFIG_OPTIONS || exit 1

make

make install DESTDIR=$PKG

# Unless things start whining later that this libtool junk is
# required for some reason, out it goes.  Especially in /lib,
# where probably nothing would find it anyway.
rm -f $PKG/lib${LIBDIRSUFFIX}/*.la

# .so links should really go in /usr/lib:
rm $PKG/lib${LIBDIRSUFFIX}/*.so
mkdir -p $PKG/usr/lib${LIBDIRSUFFIX}
# sanity check
if [ ! -L $PKG/lib${LIBDIRSUFFIX}/libsysfs.so.2 ]; then
  exit 1
fi
( cd $PKG/usr/lib${LIBDIRSUFFIX} ; ln -sf /lib${LIBDIRSUFFIX}/libsysfs.so.2 libsysfs.so )

( cd $PKG
  find . | xargs file | grep "executable" | grep ELF | cut -f 1 -d : | xargs strip --strip-unneeded 2> /dev/null
  find . | xargs file | grep "shared object" | grep ELF | cut -f 1 -d : | xargs strip --strip-unneeded 2> /dev/null
)

mkdir -p $PKG/usr/doc/sysfsutils-$VERSION
cp -a \
  AUTHORS COPYING CREDITS NEWS README TODO \
  docs/* \
  $PKG/usr/doc/sysfsutils-$VERSION

# Compress and/or relink manpages:
if [ -d $PKG/usr/man ]; then
  ( cd $PKG/usr/man
    for manpagedir in $(find . -type d -name "man*") ; do
      ( cd $manpagedir
        for eachpage in $( find . -type l -maxdepth 1) ; do
          ln -s $( readlink $eachpage ).gz $eachpage.gz
          rm $eachpage
        done
        gzip -9 *.?
      )
    done
  )
fi

mkdir $PKG/install
cat $CWD/slack-desc > $PKG/install/slack-desc

cd $PKG
requiredbuilder -v -y -s $RELEASEDIR $PKG
makepkg -c n -l y $RELEASEDIR/$NAME-$VERSION-$ARCH-$BUILD.txz

rm -rf $TMP
fi
