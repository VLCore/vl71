if [ -x /usr/bin/update-desktop-database ]; then
  ./usr/bin/update-desktop-database -q usr/share/applications
fi

nv=$(lspci | grep -i "nvidia")
if [ "x$nv" != "x" ]; then
	echo "NVIDIA hardware detected.  Activating driver."
	/usr/sbin/nvidia-switch --install
else
	echo ""
	echo "NVIDIA Legacy Driver"
	echo ""
	echo "Your driver has been installed.  To use it,"
	echo "You must execute '/usr/sbin/nvidia-switch --install'"
	echo ""
	echo ""
	echo "See @README_PATH@ for more info"


fi

